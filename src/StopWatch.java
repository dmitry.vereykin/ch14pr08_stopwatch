/**
 * Created by Dmitry Vereykin on 7/29/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

public class StopWatch extends JApplet {
    private JButton startButton;
    private JButton stopButton;
    private JLabel watch;

    private long startTime;
    private boolean running;
    private Timer timer;

    int seconds;
    String secondsStr = "";


    public void init() {
        setLayout(new BorderLayout());

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());

        watch = new JLabel("Running:  0 sec.");
        watch.setFont(new Font("TimesNewRoman", Font.BOLD, 38));
        watch.setBackground(Color.white);

        topPanel.add(watch);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout());

        startButton = new JButton("Start");
        startButton.addActionListener(new ButtonListener());
        stopButton = new JButton("Stop");
        stopButton.addActionListener(new ButtonListener());

        bottomPanel.add(startButton);
        bottomPanel.add(stopButton);

        this.getContentPane().add(topPanel, BorderLayout.CENTER);
        this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);

    }

    public void paint(Graphics g) {
        g.drawString(secondsStr, 25, 25);

    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            double time = (System.currentTimeMillis() - startTime) / 1000.0;
            watch.setText("Running:  " + time + " sec.");
            watch.setForeground(Color.RED);

            if (e.getSource() == startButton) {
                if (!running) {
                    running = true;
                    startTime = e.getWhen();
                    watch.setText("Running:  0 sec.");
                    if (timer == null) {
                        timer = new Timer(100, this);
                        timer.start();
                    }
                    else
                        timer.restart();
                }
            } else if (e.getSource() == stopButton) {
                timer.stop();
                running = false;
                long endTime = e.getWhen();
                double seconds = (endTime - startTime) / 1000.0;
                watch.setForeground(Color.BLACK);
                watch.setText("Time: " + seconds + " sec.");
            }
        }
    }

}
